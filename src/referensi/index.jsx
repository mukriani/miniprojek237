import React from 'react';
import FormInput from './forminputreferensi';
//import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import ReferensiService from '../service/referensiservice';
import Modal from 'react-bootstrap/Modal';
//import { config } from '../config/config';
import { Table } from 'react-bootstrap';




class Referensi extends React.Component {
    referensiModel = {
        id: 0,
        created_by:"",
        created_on:"",
        modified_by:"",
        modified_on:"",
        deleted_by:"",
        deleted_on:"",
        is_delete:"",
        biodata_id:"",
        name:"",
        position:"",
        address_phone:"",
        relation: ""
    }

    constructor() {
        super();
        this.state = {
            ListReferensi:[],
            ListTampil:[],
            ReferensiModel: this.ReferensiModel,
            hidden: true,
            mode: "",
            openmodal: false,
            open_modal: false,
            Created_by: "m"
        }
    }

    componentDidMount() {
        this.loadList();
        this.loadListOrder();
    }


    loadList = async () => {
        const respon = await OrderDetailService.getAllreferensi();
        alert(respon.result)
        if (respon.success) {
            this.setState({
                ListReferensi: respon.result
            })
        }
        alert(this.state.ListRiferensi)
    }
   
   

    onSelect = (ref) => {
        alert(JSON.stringify(ref))
        const { ListReferensi } = this.state;

        let idx = ListReferensi.findIndex(i => i.id === ref.id);

        if (idx != -1) {
            ListReferensi.splice(idx, 1);
        } else {
            this.setState({

                Referensi: {
                    id: ref.id,
                    name: ref.name
                }

            })
        }
        alert(JSON.stringify(ListReferensi))
    }

    hendleopen = () => {
        this.setState({
            open_modal: true,
            ReferensiModel: this.ReferensiModel,
            mode: 'more'
        })
    }

    cancelHendler = async () => {
        // const respon = await BarangService.getAll();
        // if (respon.success) {
        this.setState({
            // BarangModel: respon.result,
            open_modal: false

        });

    }
    onSave = async () => {
        const ListReferensi = {
            ListReferensi: this.state.ListReferensi
        }
        const respons = await ReferensiService.post(ListReferensi);
        if (respons.success) {
            alert('Success : ' + respons.result)
            this.loadList();
            this.setState({
                open_modal: false
            });
        }
        else {
            alert('Error : ' + respons.result)
        }
        alert(JSON.stringify(ListReferensi))
    }

    onSetQty = () => {
        const { ListReferensi, Referensi } = this.state;
        ListReferensi.push(Referensi);
        this.setState({
            modalqty: false
        })
    }
    onInputQty = name => ({ target: { value } }) => {
        alert(name + value)
        this.setState({
            Referensi: {
                ...this.state.Referensi,
                [name]: value
            }
        })
        // onNewOrder = async (filter) => {
        //     const respon = await BarangService.getAll(filter);
        //     if (respon.succes) {
        //         this.setState({
        //             ListBarang: respon.result,
        //             openmodal: true,
        //             Barang: {},
        //             ListOrder: []
        //         })
        //     }
        // }
    }



    render() {
        const { ListReferensi, modalqty, ListTampil, onSetQty, Referensi, cancelHendler, onInputQty, ReferensiModel, hidden, openmodal, open_modal, title, onSave, mode } = this.state;
        return (
            <div>
                <h5>List Referensi</h5>
                <FormInput
                    ListReferensi={ListReferensi}
                    onSetQty={this.onSetQty}
                    hendleopen={this.hendleopen}
                    ReferensiModel={ReferensiModel}
                    hidden={hidden}
                    title={mode}
                    Referensi={Referensi}
                    open_modal={open_modal}
                    selectedHandler={this.selectedHandler}
                    mode={mode}
                    modalqty={modalqty}
                    onSelect={this.onSelect}
                    onInputQty={this.onInputQty}
                    cancelHendler={this.cancelHendler}
                    onSave={this.onSave}
                />
                {
                    ListReferensi.map(ref => {
                        return(
                            <div>
                        {/* <h3>Kode Transaksi: {trx.Kode_Transaksi}</h3> */}
                            <Table stripped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                      
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        ListTampil.map(ref => {
                                        
                                                return (
                                                    <tr>
                                                        <td>{ref.name}</td>
                                                       
                                                       
                                                    </tr>
                                                )
                                        })
                                    }
                                </tbody>

                            </Table>
            </div>
        )
    })
}
</div >
)}
}


export default Referensi;
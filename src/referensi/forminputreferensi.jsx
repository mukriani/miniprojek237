import React from 'react';
import Form from 'react-bootstrap/Form';
//import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.min.css';
import Select from 'react-select';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Referensi from '../referensi';
import { ModalBody } from 'react-bootstrap';
//import OrderDetailService from '../service/BarangService';

class FormInput extends React.Component {


    render() {
        const { ListReferensi, ListTampil,onSave, onInputQty,modalqty, onSetQty, onSelect, hidden, openmodal, hendleopen, Barang, cancelHendler, open_modal, title, kategoriOption, mode } = this.props;
        return (
            <div>
                <Button variant="primary" onClick={hendleopen}>more</Button>
                <Modal show={open_modal} style={{ opacity: 1 }}>
                    <Modal.Header style={{ backgroundColor: "lightblue" }}>
                        <Modal.Title>more</Modal.Title>
                       
                            </Modal.Header>
                            <Modal.Body>

                                <label className="Form-label">Referensi</label>
                                <input type="text" className='form-control' placeholder="Kode Transaksi akan" />
                                <Table stripped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            ListReferensi.map(brg => {
                                                return (
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" className='form-check-input' onClick={() => onSelect(brg)} />
                                                        </td>
                                                        <td>{ref.name}</td>
                                                      


                                                    </tr>

                                                )
                                            })
                                        }
                                    </tbody>
                                </Table>
                        </Modal.Body>
                            <Modal.Footer>
                                <div className="modal-Footer">
                                    <button type="button" className="btn btn-primary" onClick={onSave}>Order</button>&emsp;
                            <button type="button" className="danger" onClick={cancelHendler}>Cancel</button>
                                </div>
                            </Modal.Footer>
                        </Modal>


                {/* <Button variant="primary" onClick={hendleopen}>NewOrder</Button> */}
                <Modal show={modalqty} style={{ opacity: 1 }}>
                    <Modal.Header style={{ backgroundColor: "lightblue" }}>
                        <Modal.Title>Jumlah Beli</Modal.Title>

                    </Modal.Header>
                    <Modal.Body>
                        <label>Stok:{Barang.Qty}</label><br />
                        <label>Harga:{Barang.Harga}</label><br />
                        <input type="text" id="qtybeli" onChange={onInputQty("qtybeli")}></input>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="warning"onClick={cancelHendler}>Cancel</Button>
                        <Button variant="primary" onClick={onSetQty}>Ok</Button>
                    </Modal.Footer>
                </Modal>

                
            </div>
        )
    }
}

export default FormInput                   